--cls_complexity_note
CREATE TABLE IF NOT EXISTS cls_complexity_note
(
    id BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(50),
    note TEXT,
    code VARCHAR(50)
);

INSERT INTO cls_complexity_note (name, note, code) VALUES
('Очень легкий', 'Выполнение задачи требует до 5 минут', 'EASY'),
('Легкий', 'Выполнение задачи до часа', 'LIGHT'),
('Средний', 'Выполнение задачи до 2 часов', 'AVERAGE'),
('Сложнее среднего', 'Выполнение задачи до 3.5 часов', 'MORE_AVERAGE'),
('Сложный', 'Выполнение задачи до 5 часов', 'DIFFICULT');

--cls_complexity_purpose
CREATE TABLE IF NOT EXISTS cls_complexity_purpose
(
    id BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(50),
    note TEXT,
    code VARCHAR(50)
);

INSERT INTO cls_complexity_purpose (name, note, code) VALUES
('Легкий', 'Выполнение задачи до 6 часов', 'LIGHT'),
('Средний', 'Выполнение задачи до 10 часов', 'AVERAGE'),
('Сложнее среднего', 'Выполнение задачи до 14 часов', 'MORE_AVERAGE'),
('Сложный', 'Выполнение задачи до 20 часов и более', 'DIFFICULT');

-- cls_priority
CREATE TABLE IF NOT EXISTS cls_priority
(
    id BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(50),
    code VARCHAR(50)
);

INSERT INTO cls_priority (name, code) VALUES
('Низкий', 'LOW'),
('Средний', 'MEDIUM'),
('Высокий', 'HIGH'),
('Очень высокий', 'VERY_HIGH');

--cls_type_direction
CREATE TABLE IF NOT EXISTS cls_type_direction
(
    id BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(50),
    code VARCHAR(50)
);

INSERT INTO cls_type_direction(name, code) VALUES
('Программирование', 'PROGRAMMING'),
('Формирование привычек', 'HABIT_FORMATION'),
('Домашние дела', 'HOME'),
('Менеджмент', 'MANAGEMENT');

--cls_direction
CREATE TABLE IF NOT EXISTS cls_direction
(
    id BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(50),
    time_create TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    id_type_direction INTEGER
        CONSTRAINT fk_cls_direction_cls_type_direction
        REFERENCES cls_type_direction
);

--cls_purpose
CREATE TABLE IF NOT EXISTS cls_purpose
(
    id BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(100) NOT NULL,
    results VARCHAR(150) NOT NULL,
    id_complexity INTEGER
        CONSTRAINT fk_cls_purpose_cls_complexity_purpose
        REFERENCES cls_complexity_purpose,
    id_priority INTEGER
        CONSTRAINT fk_cls_purpose_cls_priority
        REFERENCES cls_priority,
    id_direction INTEGER
        CONSTRAINT fk_cls_purpose_cls_direction
        REFERENCES cls_direction,
    time_create TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    time_start TIMESTAMP NOT NULL,
    deadline TIMESTAMP NOT NULL,
    reasons_for_doing TEXT,
    is_done BOOLEAN DEFAULT FALSE,
    wasted_time VARCHAR(30),
    is_suspended BOOLEAN DEFAULT FALSE,
    time_suspended TIMESTAMP,
    is_deleted BOOLEAN DEFAULT FALSE,
    cause_deleted TEXT
);

--cls_note
CREATE TABLE IF NOT EXISTS cls_note
(
    id BIGSERIAL PRIMARY KEY,
    "name" VARCHAR(100) NOT NULL,
    results VARCHAR(150) NOT NULL,
    id_complexity INTEGER
        CONSTRAINT fk_cls_note_cls_complexity_note
        REFERENCES cls_complexity_note,
    id_priority INTEGER
        CONSTRAINT fk_cls_note_cls_priority
        REFERENCES cls_priority,
    id_purpose INTEGER
        CONSTRAINT fk_cls_note_cls_purpose
        REFERENCES cls_purpose,
    time_create TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    date_start TIMESTAMP NOT NULL,
    time_start TIME NOT NULL,
    deadline TIME NOT NULL,
    reasons_for_doing TEXT,
    is_done BOOLEAN DEFAULT FALSE,
    wasted_time VARCHAR(30),
    is_suspended BOOLEAN DEFAULT FALSE,
    time_suspended TIME,
    is_deleted BOOLEAN DEFAULT FALSE,
    cause_deleted TEXT
);