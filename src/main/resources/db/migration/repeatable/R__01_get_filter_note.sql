CREATE OR REPLACE FUNCTION get_filter_note(p_search_name_result TEXT, p_id_priority bigint, p_id_complexity bigint,
                                           p_id_purpose bigint, p_date_start TIMESTAMP WITHOUT TIME ZONE,
                                           p_date_before_start TIMESTAMP WITHOUT TIME ZONE, p_date_after_start TIMESTAMP WITHOUT TIME ZONE,
                                           p_time_start TIME, p_time_before_start TIME, p_time_after_start TIME, p_deadline TIME, p_before_deadline TIME,
                                           p_after_deadline TIME, p_is_default boolean, p_is_done boolean, p_is_suspended boolean, p_is_deleted boolean)
    RETURNS SETOF cls_note
    LANGUAGE PLPGSQL
AS
$$
BEGIN
    RETURN QUERY
        WITH note AS (
            SELECT cn.*
            FROM cls_note AS cn
                INNER JOIN cls_complexity_note AS ccn
                  ON ccn.id = cn.id_complexity
                INNER JOIN cls_priority AS cp
                  ON cp.id = cn.id_priority
                INNER JOIN cls_purpose AS c
                  ON c.id = cn.id_purpose
                WHERE (cp.id = p_id_priority OR p_id_priority IS NULL)
                  AND (cp.id = p_id_complexity OR p_id_complexity IS NULL)
                  AND (cp.id = p_id_purpose OR p_id_purpose IS NULL)
                )
        SELECT *
        FROM note AS n
        WHERE (n.name ILIKE p_search_name_result
               OR  n.results ILIKE p_search_name_result
               OR  p_search_name_result IS NULL)
          AND (n.date_start = p_date_start OR p_date_start IS NUll)
          AND (n.date_start < p_date_before_start OR p_date_before_start IS NUll)
          AND (n.date_start > p_date_after_start OR p_date_after_start IS NUll)
          AND (n.time_start = p_time_start OR p_time_start IS NUll)
          AND (n.time_start < p_time_before_start OR p_time_before_start IS NUll)
          AND (n.time_start > p_time_after_start OR p_time_after_start IS NUll)
          AND (n.deadline = p_deadline OR p_deadline IS NUll)
          AND (n.deadline < p_before_deadline OR p_before_deadline IS NUll)
          AND (n.deadline > p_after_deadline OR p_after_deadline IS NUll)
          AND n.is_done = p_is_done
          AND n.is_suspended = p_is_suspended
          AND n.is_deleted = p_is_deleted
          AND n.is_default = p_is_default;
END;
$$;