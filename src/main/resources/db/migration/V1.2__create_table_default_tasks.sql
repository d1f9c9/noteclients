--cls_note
ALTER TABLE IF EXISTS cls_note ADD COLUMN IF NOT EXISTS is_default BOOLEAN DEFAULT FALSE;

CREATE TABLE IF NOT EXISTS cls_role
(
    id BIGSERIAL PRIMARY KEY,
    "name" TEXT NOT NULL,
    code TEXT  NOT NULL
);

CREATE TABLE IF NOT EXISTS cls_privilege
(
    id BIGSERIAL PRIMARY KEY,
    "name" TEXT NOT NULL,
    code TEXT NOT NULL
);


CREATE TABLE IF NOT EXISTS cls_user
(
    id BIGSERIAL PRIMARY KEY,
    "name" TEXT NOT NULL,
    login TEXT NOT NUlL,
    password TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS reg_user_role
(
    id BIGSERIAL PRIMARY KEY,
    id_user INTEGER
        CONSTRAINT fk_reg_user_role_cls_user
        REFERENCES cls_user,
    id_role INTEGER
        CONSTRAINT fk_reg_user_role_cls_role
        REFERENCES cls_role
);

CREATE TABLE IF NOT EXISTS reg_user_privilege
(
    id BIGSERIAL PRIMARY KEY,
    id_role INTEGER
        CONSTRAINT fk_reg_user_privilege_cls_role
        REFERENCES cls_role,
    id_user INTEGER
        CONSTRAINT fk_reg_user_privilege_cls_privilege
        REFERENCES cls_privilege
);

ALTER TABLE IF EXISTS cls_user ADD COLUMN IF NOT EXISTS mail TEXT NOT NULL;