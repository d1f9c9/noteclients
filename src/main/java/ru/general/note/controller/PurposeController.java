package ru.general.note.controller;

import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.general.note.dto.ClsPurposeDto;
import ru.general.note.service.PurposeService;

import java.util.List;

@RestController
@CrossOrigin(origins = "https://note-client-rust.vercel.app", maxAge = 3600)
@RequestMapping("purpose")
public class PurposeController {
    @Autowired
    PurposeService purposeService;

    @GetMapping
    List<ClsPurposeDto> getAll() {
        List<ClsPurposeDto> purposes = purposeService.getAll();

        return purposes;
    }

    @GetMapping("/{id_purpose}/")
    ClsPurposeDto getAll(@PathVariable("id_purpose") Long idPurpose) {
        ClsPurposeDto purpose = purposeService.getPurpose(idPurpose);

        return purpose;
    }


    @PostMapping
    ResponseEntity<String> addNote(@RequestBody @Parameter(description = "Глобальная задача") ClsPurposeDto purpose) {
        purposeService.addOrUpdatePurpose(purpose);

        return ResponseEntity.ok("Добавлено");
    }

    @PutMapping
    ResponseEntity<String> updateNote(@RequestBody ClsPurposeDto purpose) {
        purposeService.addOrUpdatePurpose(purpose);

        return ResponseEntity.ok("Добавлено");
    }

    @DeleteMapping
    ResponseEntity<String> deleteNote(@RequestBody ClsPurposeDto purpose) {
        purposeService.deletePurpose(purpose);

        return ResponseEntity.ok("Добавлено");
    }
}
