package ru.general.note.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.general.note.dto.ClsComplexityNoteDto;
import ru.general.note.dto.ClsComplexityPurposeDto;
import ru.general.note.service.ComplexityService;

import java.util.List;

@RestController
@RequestMapping("complexity")
@CrossOrigin(origins = "https://note-client-rust.vercel.app", maxAge = 3600)
public class ComplexityController {
    @Autowired
    ComplexityService complexityService;

    @GetMapping("note")
    List<ClsComplexityNoteDto> getComplexityNote() {
        List<ClsComplexityNoteDto> complexityNote = complexityService.getAllComplexityNote();

        return complexityNote;
    }

    @GetMapping("purpose")
    List<ClsComplexityPurposeDto> getComplexityPurpose() {
        List<ClsComplexityPurposeDto> complexityNote = complexityService.getAllComplexityPurpose();

        return complexityNote;
    }
}
