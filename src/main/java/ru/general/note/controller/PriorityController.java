package ru.general.note.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.general.note.dto.ClsPriorityDto;
import ru.general.note.service.PriorityService;

import java.util.List;

@RestController
@CrossOrigin(origins = "https://note-client-rust.vercel.app", maxAge = 3600)
@RequestMapping("priority")
public class PriorityController {
    @Autowired
    PriorityService priorityService;

    @GetMapping
    List<ClsPriorityDto> getAll() {
        List<ClsPriorityDto> priorities = priorityService.getAll();

        return priorities;
    }

}
