package ru.general.note.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.general.note.dto.ClsUserDto;
import ru.general.note.model.ClsUser;
import ru.general.note.repository.ClsUserRepo;

import javax.persistence.EntityExistsException;

@RestController
@CrossOrigin(origins = "https://note-client-rust.vercel.app", maxAge = 3600)
@RequestMapping("user")
public class UserController {
    @Autowired
    ClsUserRepo clsUserRepo;

    @PostMapping("registration")
    ResponseEntity<ClsUserDto> registration(@RequestBody ClsUserDto newUserCls) {
        ClsUser user = clsUserRepo.findByMail(newUserCls.getMail()).orElse(null);
        ClsUserDto userDto = null;
        if (user == null) {
            ClsUser newUser = ClsUser.builder()
                    .login(newUserCls.getLogin())
                    .mail(newUserCls.getMail())
                    .name(newUserCls.getLogin())
                    .password(newUserCls.getPassword())
                    .build();

            clsUserRepo.save(newUser);
        } else {
            throw new EntityExistsException("User already exists");
        }

        return ResponseEntity.ok(newUserCls);
    }

    @PutMapping
    ResponseEntity<ClsUserDto> update(@RequestBody ClsUserDto newUserCls) {
        ClsUser user = clsUserRepo.findByMail(newUserCls.getMail()).orElse(null);
        ClsUserDto userDto = null;
        if (user != null) {
            ClsUser newUser = ClsUser.builder()
                    .login(newUserCls.getLogin())
                    .mail(newUserCls.getMail())
                    .password(newUserCls.getPassword())
                    .build();

            clsUserRepo.save(newUser);
        } else {
            throw new EntityExistsException("User already exists");
        }

        return ResponseEntity.ok(newUserCls);
    }

    @PostMapping("authorization")
    ClsUser authorization(@RequestBody ClsUserDto user) {
        var checkUser = clsUserRepo.findByLoginAndPassword(user.getLogin(), user.getPassword()).orElse(null);

        return checkUser;
    }
}
