package ru.general.note.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.general.note.dto.ClsTypeDirectionDto;
import ru.general.note.service.TypeDirectionService;

import java.util.List;

@RestController
@CrossOrigin(origins = "https://note-client-rust.vercel.app", maxAge = 3600)
@RequestMapping("direction/type")
public class TypeDirectionController {
    @Autowired
    TypeDirectionService typeDirectionService;

    @GetMapping
    List<ClsTypeDirectionDto> getDirection() {
        List<ClsTypeDirectionDto> types = typeDirectionService.getAll();

        return types;
    }
}

