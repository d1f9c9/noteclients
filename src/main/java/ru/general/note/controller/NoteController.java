package ru.general.note.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.general.note.dto.ClsNoteDto;
import ru.general.note.dto.ClsNoteFilterDto;
import ru.general.note.service.NoteService;

import java.util.List;

@RestController
@CrossOrigin(origins = "https://note-client-rust.vercel.app", maxAge = 3600)
@RequestMapping("note")
public class NoteController {
    @Autowired
    NoteService noteService;

    @GetMapping
    List<ClsNoteDto> getAll() {
        List<ClsNoteDto> notes = noteService.getAll();

        return notes;
    }

    @GetMapping("default")
    List<ClsNoteDto> getAllDefault() {
        List<ClsNoteDto> notes = noteService.getAllDefault();

        return notes;
    }

    @GetMapping("today")
    List<ClsNoteDto> getForToday() {
        List<ClsNoteDto> notes = noteService.getForToday();

        return notes;
    }

    @GetMapping("tomorrow")
    List<ClsNoteDto> getForTomorrow() {
        List<ClsNoteDto> notes = noteService.getForTomorrow();

        return notes;
    }

    @GetMapping("/{id_note}/")
    ClsNoteDto getAll(@PathVariable("id_note") Long idNote) {
        ClsNoteDto notes = noteService.getNote(idNote);

        return notes;
    }

    @PostMapping("filter")
    List<ClsNoteDto> getFiltered(@RequestBody ClsNoteFilterDto filter) {
        List<ClsNoteDto> notes = noteService.getFilter(filter);

        return notes;
    }

    @PostMapping
    ResponseEntity<String> addNote(@RequestBody ClsNoteDto note) {
        noteService.addNote(note);

        return ResponseEntity.ok("Добавлено");
    }

    @PostMapping("add/default")
    void addDefaultNotes(@RequestBody List<ClsNoteDto> notes) {
        noteService.addDefaultToDay(notes);
    }

    @PutMapping
    ResponseEntity<String> updateNote(@RequestBody ClsNoteDto note) {
        noteService.updateNote(note);

        return ResponseEntity.ok("Добавлено");
    }

    @DeleteMapping("/{id_note}")
    ResponseEntity<String> deleteNote(@PathVariable("id_note") Long idNote, @RequestParam("is_deleted") Boolean isDeleted) {
        noteService.deleteNote(idNote, isDeleted);

        return ResponseEntity.ok("Добавлено");
    }

    @PutMapping("done/{id_note}")
    ResponseEntity<String> doneNote(@PathVariable("id_note") Long idNote, @RequestParam("is_done") Boolean isDone) {
        noteService.doneNote(idNote, isDone);

        return ResponseEntity.ok("Добавлено");
    }
}

