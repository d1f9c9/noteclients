package ru.general.note.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.general.note.dto.ClsDirectionDto;
import ru.general.note.service.DirectionService;
import ru.general.note.service.DirectionServiceImpl;

import java.util.List;

@RestController
@CrossOrigin(origins = "https://note-client-rust.vercel.app", maxAge = 3600)
@RequestMapping("direction")
public class DirectionController {
    @Autowired
    DirectionService directionService;

    @GetMapping
    List<ClsDirectionDto> getDirection() {
        List<ClsDirectionDto> directionDto = directionService.getDirection();

        return directionDto;
    }

    @PostMapping
    ResponseEntity<String> addDirection(@RequestBody ClsDirectionDto direction) {
        directionService.addOrUpdateDirection(direction);

        return ResponseEntity.ok("Добавлено");
    }

    @PutMapping
    ResponseEntity<String> updateDirection(@RequestBody ClsDirectionDto direction) {
        directionService.addOrUpdateDirection(direction);

        return ResponseEntity.ok("Обнавлено");
    }
}
