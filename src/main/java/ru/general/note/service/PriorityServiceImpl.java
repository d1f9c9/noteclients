package ru.general.note.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.general.note.dto.ClsPriorityDto;
import ru.general.note.model.ClsPriority;
import ru.general.note.repository.ClsPriorityRepo;

import java.util.List;

@Service
public class PriorityServiceImpl implements PriorityService {
    @Autowired
    ClsPriorityRepo clsPriorityRepo;

    public List<ClsPriorityDto> getAll() {
        List<ClsPriority> priorities = clsPriorityRepo.findAll();
        List<ClsPriorityDto> priorityDtos = priorities.stream().map(ClsPriorityDto::new).toList();

        return priorityDtos;
    }
}
