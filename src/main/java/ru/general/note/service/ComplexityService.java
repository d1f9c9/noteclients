package ru.general.note.service;

import ru.general.note.dto.ClsComplexityNoteDto;
import ru.general.note.dto.ClsComplexityPurposeDto;

import java.util.List;

public interface ComplexityService {
    List<ClsComplexityNoteDto> getAllComplexityNote();

    List<ClsComplexityPurposeDto> getAllComplexityPurpose();
}
