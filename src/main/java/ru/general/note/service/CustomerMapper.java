package ru.general.note.service;

import org.mapstruct.BeanMapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import ru.general.note.dto.ClsNoteDto;
import ru.general.note.model.ClsNote;

public interface CustomerMapper {
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateCustomerFromDto(ClsNoteDto noteDto, @MappingTarget ClsNote note);
}
