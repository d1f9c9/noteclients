package ru.general.note.service;

import ru.general.note.dto.ClsTypeDirectionDto;

import java.util.List;

public interface TypeDirectionService {
    List<ClsTypeDirectionDto> getAll();
}
