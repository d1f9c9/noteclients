package ru.general.note.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.general.note.dto.ClsTypeDirectionDto;
import ru.general.note.model.ClsTypeDirection;
import ru.general.note.repository.ClsTypeDirectionRepo;

import java.util.List;

@Service
public class TypeDirectionServiceImpl implements TypeDirectionService {
    @Autowired
    ClsTypeDirectionRepo clsTypeDirectionRepo;

    @Override
    public List<ClsTypeDirectionDto> getAll() {
        List<ClsTypeDirection> types = clsTypeDirectionRepo.findAll();
        List<ClsTypeDirectionDto> typeDtos = types.stream().map(ClsTypeDirectionDto::new).toList();

        return typeDtos;
    }
}
