package ru.general.note.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.general.note.dto.ClsDirectionDto;
import ru.general.note.dto.ClsTypeDirectionDto;
import ru.general.note.model.ClsDirection;
import ru.general.note.model.ClsTypeDirection;
import ru.general.note.repository.ClsDirectionRepo;
import ru.general.note.repository.ClsTypeDirectionRepo;

import java.util.List;

@Service
public class DirectionServiceImpl implements DirectionService {
    @Autowired
    ClsDirectionRepo clsDirectionRepo;
    @Autowired
    ClsTypeDirectionRepo clsTypeDirectionRepo;

    @Override
    public void addOrUpdateDirection(ClsDirectionDto directionDto) {
        ClsDirection direction = new ClsDirection(directionDto);
        clsDirectionRepo.save(direction);
    }

    @Override
    public List<ClsDirectionDto> getDirection() {
        List<ClsDirection> direction = clsDirectionRepo.findAll();
        List<ClsDirectionDto> directionDtos = direction.stream().map(dire -> {
            ClsTypeDirection type = clsTypeDirectionRepo.findById(dire.getTypeDirection().getId()).orElse(null);
            ClsTypeDirectionDto typeDto = new ClsTypeDirectionDto(type);
            ClsDirectionDto directionDto = new ClsDirectionDto(dire, typeDto);

            return directionDto;
        }).toList();

        return directionDtos;
    }
}
