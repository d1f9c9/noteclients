package ru.general.note.service;

import ru.general.note.dto.ClsNoteDto;
import ru.general.note.dto.ClsPurposeDto;

import java.util.List;

public interface PurposeService {
    List<ClsPurposeDto> getAll();
    ClsPurposeDto getPurpose(Long idPurpose);
    void addOrUpdatePurpose(ClsPurposeDto purpose);

    void deletePurpose(ClsPurposeDto purpose);
}
