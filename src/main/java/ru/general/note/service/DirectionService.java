package ru.general.note.service;

import ru.general.note.dto.ClsDirectionDto;

import java.util.List;

public interface DirectionService {
    void addOrUpdateDirection(ClsDirectionDto directionDto);

    List<ClsDirectionDto> getDirection();
}
