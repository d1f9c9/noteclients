package ru.general.note.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.general.note.dto.ClsNoteDto;
import ru.general.note.dto.ClsNoteFilterDto;
import ru.general.note.dto.ClsTypeDirectionDto;
import ru.general.note.model.ClsNote;
import ru.general.note.model.ClsPriority;
import ru.general.note.model.ClsPurpose;
import ru.general.note.model.ClsTypeDirection;
import ru.general.note.repository.ClsNoteRepo;
import ru.general.note.repository.ClsTypeDirectionRepo;
import ru.general.note.utils.DateUtils;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {
    @Autowired
    ClsNoteRepo clsNoteRepo;
    @Autowired
    ClsTypeDirectionRepo clsTypeDirectionRepo;

    @Override
    public List<ClsNoteDto> getAll() {
        List<ClsNote> notes = clsNoteRepo.findAll();
        List<ClsNoteDto> noteDtos = notes.stream().map(note -> {
            ClsNoteDto noteDto = null;

            if (note.getPurpose() != null) {
                ClsTypeDirection typeDirection = null;

                if (note.getPurpose().getDirection() != null) {
                    typeDirection = clsTypeDirectionRepo.findById(note.getPurpose().getDirection().getTypeDirection().getId()).orElse(null);
                }
                ClsTypeDirectionDto typeDirectionDto = new ClsTypeDirectionDto(typeDirection);
                noteDto = new ClsNoteDto(note, typeDirectionDto);
            }
            return noteDto;
        }).toList();

        return noteDtos;
    }

    @Override
    public ClsNoteDto getNote(Long idNote) {
        ClsNote note = clsNoteRepo.findById(idNote).orElse(null);
        ClsTypeDirection typeDirection = clsTypeDirectionRepo.findById(note.getPurpose().getDirection().getTypeDirection().getId()).orElse(null);
        assert typeDirection != null;
        ClsTypeDirectionDto typeDirectionDto = new ClsTypeDirectionDto(typeDirection);
        ClsNoteDto noteDto = new ClsNoteDto(note, typeDirectionDto);

        return noteDto;
    }

    private List<ClsNoteDto> getNoteDto(List<ClsNote> notes) {
        List<ClsNoteDto> noteDtos = notes.stream().map(note -> {
            ClsNoteDto noteDto = null;

            if (note.getPurpose() != null) {
                ClsTypeDirection typeDirection = null;

                if (note.getPurpose().getDirection() != null) {
                    typeDirection = clsTypeDirectionRepo.findById(note.getPurpose().getDirection().getTypeDirection().getId()).orElse(null);
                }
                ClsTypeDirectionDto typeDirectionDto = new ClsTypeDirectionDto(typeDirection);
                noteDto = new ClsNoteDto(note, typeDirectionDto);
            }

            return noteDto;
        }).toList();

        return noteDtos;
    }

    @Override
    public List<ClsNoteDto> getFilter(ClsNoteFilterDto filter) {
        List<ClsNote> notes = clsNoteRepo.getFilteredNote(filter.getSearchNameResult(), filter.getIdPriority(), filter.getIdComplexity(),
                                                          filter.getIdPurpose(), filter.getDateStart(), filter.getDateBeforeStart(),
                                                          filter.getDateAfterStart(), filter.getTimeStart(), filter.getTimeBeforeStart(),
                                                          filter.getTimeAfterStart(), filter.getDeadline(), filter.getBeforeDeadline(),
                                                          filter.getAfterDeadline(), filter.getIsDefault(), filter.getIsDone(),
                                                          filter.getIsSuspended(), filter.getIsDeleted());

        List<ClsNoteDto> noteDtos = getNoteDto(notes);

        return noteDtos;
    }

    @Override
    public List<ClsNoteDto> getAllDefault() {
        List<ClsNote> notes = clsNoteRepo.findByIsDefault(true);
        List<ClsNoteDto> noteDtos = getNoteDto(notes);

        return noteDtos;
    }

    @Override
    public List<ClsNoteDto> getForToday() {
        Date now = new Date();
        Date dateStart = DateUtils.atStartOfDay(now);
        Date dateEnd = DateUtils.atEndOfDay(now);
        List<ClsNote> notes = clsNoteRepo.findByDateStartBetweenOrderByTimeStart(dateStart, dateEnd);
        List<ClsNoteDto> noteDtos = getNoteDto(notes);

        return noteDtos;
    }

    @Override
    public List<ClsNoteDto> getForTomorrow() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime tomorrow = now.plusDays(1);
        Date tomorrowStart = DateUtils.atStartOfDay(DateUtils.localDateTimeToDate(tomorrow));
        Date tomorrowEnd = DateUtils.atEndOfDay(DateUtils.localDateTimeToDate(tomorrow));

        List<ClsNote> notes = clsNoteRepo.findByDateStartBetweenOrderByTimeStart(tomorrowStart, tomorrowEnd);
        List<ClsNoteDto> noteDtos = getNoteDto(notes);

        return noteDtos;
    }

    private ClsNote buildNewNote(ClsNoteDto noteDto, boolean isNewDate) {
        ClsNote note = new ClsNote().toBuilder()
                .name(noteDto.getName())
                .results(noteDto.getResults())
                .complexity(noteDto.getComplexity())
                .priority(new ClsPriority(noteDto.getPriority()))
                .purpose(noteDto.getPurpose() != null ? new ClsPurpose(noteDto.getPurpose()) : null)
                .timeCreate(new Timestamp(System.currentTimeMillis()))
                .dateStart(noteDto.getDateStart())
                .timeStart(noteDto.getTimeStart())
                .deadline(noteDto.getDeadline())
                .reasonsForDoing(noteDto.getReasonsForDoing())
                .isDone(noteDto.getIsDone())
                .wastedTime(noteDto.getWastedTime())
                .isSuspended(noteDto.getIsSuspended())
                .timeSuspended(noteDto.getTimeSuspended())
                .isDeleted(noteDto.getIsDeleted())
                .isDefault(noteDto.getIsDefault())
                .causeDeleted(noteDto.getCauseDeleted())
                .build();

        return note;
    }

    @Override
    public void addNote(ClsNoteDto noteDto) {
        ClsNote note = buildNewNote(noteDto, false);

        clsNoteRepo.save(note);
    }

    @Override
    public void addDefaultToDay(List<ClsNoteDto> noteDtos) {
        List<ClsNote> notes = noteDtos.stream().map(note -> buildNewNote(note, true)).toList();
        clsNoteRepo.saveAll(notes);
    }

    @Override
    public void updateNote(ClsNoteDto noteDto) {
        ClsNote note = clsNoteRepo.findById(noteDto.getId()).orElse(null);

        ClsNote newNote = new ClsNote().toBuilder()
                .id(noteDto.getId())
                .name(noteDto.getName())
                .results(noteDto.getResults())
                .complexity(noteDto.getComplexity())
                .priority(new ClsPriority(noteDto.getPriority()))
                .purpose(noteDto.getPurpose() != null ? new ClsPurpose(noteDto.getPurpose()) : null)
                .dateStart(noteDto.getDateStart())
                .timeStart(noteDto.getTimeStart())
                .timeCreate(note != null ? note.getTimeCreate() : null)
                .deadline(noteDto.getDeadline())
                .reasonsForDoing(noteDto.getReasonsForDoing())
                .isDone(noteDto.getIsDone())
                .wastedTime(noteDto.getWastedTime())
                .isSuspended(noteDto.getIsSuspended())
                .timeSuspended(noteDto.getTimeSuspended())
                .isDeleted(noteDto.getIsDeleted())
                .isDefault(noteDto.getIsDefault())
                .causeDeleted(noteDto.getCauseDeleted())
                .build();

        clsNoteRepo.save(newNote);
    }

    @Override
    public void deleteNote(Long idNote, Boolean isDeleted) {
        ClsNote note = clsNoteRepo.findById(idNote).orElseThrow();
        note.setIsDeleted(isDeleted);
        clsNoteRepo.save(note);
    }

    @Override
    public void doneNote(Long idNote, Boolean isDone) {
        ClsNote note = clsNoteRepo.findById(idNote).orElseThrow();
        note.setIsDone(isDone);
        clsNoteRepo.save(note);
    }
}
