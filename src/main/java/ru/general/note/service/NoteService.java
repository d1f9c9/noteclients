package ru.general.note.service;

import ru.general.note.dto.ClsNoteDto;
import ru.general.note.dto.ClsNoteFilterDto;

import java.util.List;

public interface NoteService {
    List<ClsNoteDto> getAll();
    ClsNoteDto getNote(Long idNote);
    List<ClsNoteDto> getFilter(ClsNoteFilterDto filter);
    List<ClsNoteDto> getAllDefault();
    List<ClsNoteDto> getForToday();
    List<ClsNoteDto> getForTomorrow();
    void addNote(ClsNoteDto noteDto);
    void addDefaultToDay(List<ClsNoteDto> noteDto);
    void updateNote(ClsNoteDto noteDto);
    void deleteNote(Long idNote, Boolean isDeleted);
    void doneNote(Long idNote, Boolean isDone);
}
