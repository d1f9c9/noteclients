package ru.general.note.service;

import ru.general.note.dto.ClsPriorityDto;

import java.util.List;

public interface PriorityService {
    List<ClsPriorityDto> getAll();
}
