package ru.general.note.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.general.note.dto.ClsPurposeDto;
import ru.general.note.dto.ClsTypeDirectionDto;
import ru.general.note.model.ClsDirection;
import ru.general.note.model.ClsPriority;
import ru.general.note.model.ClsPurpose;
import ru.general.note.model.ClsTypeDirection;
import ru.general.note.repository.ClsComplexityPurposeRepo;
import ru.general.note.repository.ClsPurposeRepo;
import ru.general.note.repository.ClsTypeDirectionRepo;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Service
public class PurposeServiceImpl implements PurposeService {
    @Autowired
    ClsPurposeRepo clsPurposeRepo;
    @Autowired
    ClsTypeDirectionRepo clsTypeDirectionRepo;

    @Autowired
    ClsComplexityPurposeRepo clsComplexityPurposeRepo;

    @Override
    public List<ClsPurposeDto> getAll() {
        List<ClsPurpose> notes = clsPurposeRepo.findAll();
        List<ClsPurposeDto> noteDtos = notes.stream().map(purpose -> {
            ClsTypeDirection typeDirection = null;
            if (purpose.getDirection() != null) {
                typeDirection = clsTypeDirectionRepo.findById(purpose.getDirection().getTypeDirection().getId()).orElse(null);
            }
            assert typeDirection != null;
            ClsTypeDirectionDto typeDirectionDto = new ClsTypeDirectionDto(typeDirection);
            ClsPurposeDto noteDto = new ClsPurposeDto(purpose, typeDirectionDto);

            return noteDto;
        }).toList();

        return noteDtos;
    }

    @Override
    public ClsPurposeDto getPurpose(Long idPurpose) {
        ClsPurpose purpose = clsPurposeRepo.findById(idPurpose).orElse(null);
        ClsTypeDirectionDto typeDirectionDto = null;

        if(purpose.getDirection() != null) {
            ClsTypeDirection typeDirection = clsTypeDirectionRepo.findById(purpose.getDirection().getTypeDirection().getId()).orElse(null);
            typeDirectionDto = new ClsTypeDirectionDto(typeDirection);
        }

        ClsPurposeDto purposeDto = new ClsPurposeDto(purpose, typeDirectionDto);

        return purposeDto;
    }

    @Override
    public void addOrUpdatePurpose(ClsPurposeDto purposeDto) {
        ClsPurpose purpose = ClsPurpose.builder()
                .name(purposeDto.getName())
                .results(purposeDto.getResults())
                .complexity(purposeDto.getComplexity())
                .priority(new ClsPriority(purposeDto.getPriority()))
                .direction(new ClsDirection(purposeDto.getDirection()))
                .timeCreate(new Timestamp(System.currentTimeMillis()))
                .timeStart(Date.valueOf(purposeDto.getTimeStart()))
                .deadline(Date.valueOf(purposeDto.getDeadline()))
                .reasonsForDoing(purposeDto.getReasonsForDoing())
                .isDone(purposeDto.getIsDone())
                .wastedTime(purposeDto.getWastedTime())
                .isSuspended(purposeDto.getIsSuspended())
                .timeSuspended(purposeDto.getTimeSuspended())
                .isDeleted(purposeDto.getIsDeleted())
                .causeDeleted(purposeDto.getCauseDeleted())
                .build();

        clsPurposeRepo.save(purpose);
    }

    @Override
    public void deletePurpose(ClsPurposeDto purposeDto) {
        purposeDto.setIsDeleted(true);
        ClsPurpose purpose = new ClsPurpose(purposeDto);
        clsPurposeRepo.save(purpose);
    }
}
