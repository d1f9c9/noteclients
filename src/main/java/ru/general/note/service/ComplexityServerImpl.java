package ru.general.note.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.general.note.dto.ClsComplexityNoteDto;
import ru.general.note.dto.ClsComplexityPurposeDto;
import ru.general.note.model.ClsComplexityNote;
import ru.general.note.model.ClsComplexityPurpose;
import ru.general.note.repository.ClsComplexityNoteRepo;
import ru.general.note.repository.ClsComplexityPurposeRepo;

import java.util.List;

@Service
public class ComplexityServerImpl implements ComplexityService{
    @Autowired
    ClsComplexityNoteRepo clsComplexityNoteRepo;
    @Autowired
    ClsComplexityPurposeRepo clsComplexityPurposeRepo;

    public List<ClsComplexityNoteDto> getAllComplexityNote() {
        List<ClsComplexityNote> complexity = clsComplexityNoteRepo.findAll();
        List<ClsComplexityNoteDto> complexityDto = complexity.stream().map(ClsComplexityNoteDto::new).toList();

        return complexityDto;
    }

    public List<ClsComplexityPurposeDto> getAllComplexityPurpose() {
        List<ClsComplexityPurpose> complexity = clsComplexityPurposeRepo.findAll();
        List<ClsComplexityPurposeDto> complexityDto = complexity.stream().map(ClsComplexityPurposeDto::new).toList();

        return complexityDto;
    }
}
