package ru.general.note.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.general.note.model.ClsPriority;

import java.util.List;

@Repository
public interface ClsPriorityRepo extends CrudRepository<ClsPriority, Long> {
    List<ClsPriority> findAll();
}
