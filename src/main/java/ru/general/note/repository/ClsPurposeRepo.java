package ru.general.note.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.general.note.model.ClsPurpose;

import java.util.List;

@Repository
public interface ClsPurposeRepo extends CrudRepository<ClsPurpose, Long> {
    List<ClsPurpose> findAll();
}
