package ru.general.note.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.general.note.model.ClsTypeDirection;

import java.util.List;

@Repository
public interface ClsTypeDirectionRepo extends CrudRepository<ClsTypeDirection, Long> {
    List<ClsTypeDirection> findAll();
}
