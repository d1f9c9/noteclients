package ru.general.note.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.general.note.model.ClsDirection;

import java.util.List;

@Repository
public interface ClsDirectionRepo extends CrudRepository<ClsDirection, Long> {
    List<ClsDirection> findAll();
}
