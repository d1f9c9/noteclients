package ru.general.note.repository;

import lombok.Data;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.general.note.model.ClsNote;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;


@Repository
public interface ClsNoteRepo extends CrudRepository<ClsNote, Long> {
    List<ClsNote> findAll();

    List<ClsNote> findByIsDefault(boolean isDefault);

    List<ClsNote> findByDateStartBetweenOrderByTimeStart(Date timeStart, Date timeEnd);

    @Query(nativeQuery = true, value = """
        SELECT *
        FROM get_filter_note(:search_name_result::TEXT, :id_priority::BIGINT, :id_complexity::BIGINT, :id_purpose::BIGINT, :date_start::TIMESTAMP WITHOUT TIME ZONE, :date_before_start::TIMESTAMP WITHOUT TIME ZONE,
                             :date_after_start::TIMESTAMP WITHOUT TIME ZONE, :time_start::TIME, :time_before_start::TIME, :time_after_start::TIME, :deadline::TIME, :before_deadline::TIME, :after_deadline::TIME,
                             :is_default::boolean, :is_done::boolean, :is_suspended::boolean, :is_deleted::boolean)""")
    List<ClsNote> getFilteredNote(@Param("search_name_result") String SearchNameResult,
                                  @Param("id_priority") Long idPriority,
                                  @Param("id_complexity") Long idComplexity,
                                  @Param("id_purpose") Long idPurpose,
                                  @Param("date_start") LocalDateTime dataStart,
                                  @Param("date_before_start") LocalDateTime dateBeforeStart,
                                  @Param("date_after_start") LocalDateTime dateAfterStart,
                                  @Param("time_start") Time timeStart,
                                  @Param("time_before_start") Time timeBeforeStart,
                                  @Param("time_after_start") Time timeAfterStart,
                                  @Param("deadline") Time deadline,
                                  @Param("before_deadline") Time beforeDeadline,
                                  @Param("after_deadline") Time afterDeadline,
                                  @Param("is_default") boolean isDefault,
                                  @Param("is_done") boolean isDone,
                                  @Param("is_suspended") boolean isSuspended,
                                  @Param("is_deleted") boolean isDeleted);
}
