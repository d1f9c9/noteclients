package ru.general.note.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.general.note.model.ClsComplexityPurpose;

import java.util.List;

@Repository
public interface ClsComplexityPurposeRepo extends CrudRepository<ClsComplexityPurpose, Long> {
    List<ClsComplexityPurpose> findAll();
}
