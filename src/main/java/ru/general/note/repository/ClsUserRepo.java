package ru.general.note.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.general.note.model.ClsUser;

import java.util.Optional;

@Repository
public interface ClsUserRepo extends CrudRepository<ClsUser, Long> {
    Optional<ClsUser> findByMail(String mail);

    Optional<ClsUser> findByLoginAndPassword(String login, String password);
}
