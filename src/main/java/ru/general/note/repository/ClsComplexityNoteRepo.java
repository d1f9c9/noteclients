package ru.general.note.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.general.note.model.ClsComplexityNote;

import java.util.List;

@Repository
public interface ClsComplexityNoteRepo extends CrudRepository<ClsComplexityNote, Long> {
    List<ClsComplexityNote> findAll();
}
