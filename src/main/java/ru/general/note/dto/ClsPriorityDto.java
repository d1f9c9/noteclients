package ru.general.note.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.general.note.model.ClsPriority;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClsPriorityDto {
    private Long id;
    private String name;
    private String code;

    public ClsPriorityDto(@NonNull ClsPriority priority) {
        id = priority.getId();
        name = priority.getName();
        code = priority.getCode();
    }
}
