package ru.general.note.dto;

import lombok.Data;

@Data
public class ClsRoleDto {
    private Long id;
    private String name;
    private String login;
    private String password;
}
