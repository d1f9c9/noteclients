package ru.general.note.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.general.note.model.ClsComplexityNote;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClsComplexityNoteDto {
    private Long id;
    private String name;
    private String note;
    private String code;

    public ClsComplexityNoteDto(ClsComplexityNote complexity) {
        if(complexity != null) {
            id = complexity.getId();
            name = complexity.getName();
            note = complexity.getNote();
            code = complexity.getCode();
        }
    }
}
