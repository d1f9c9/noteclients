package ru.general.note.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.general.note.model.ClsDirection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClsDirectionDto {
    private Long id;
    private String name;
    private ClsTypeDirectionDto type;

    public ClsDirectionDto(@NonNull ClsDirection direction, @NonNull ClsTypeDirectionDto directionType) {
        id = direction.getId();
        name = direction.getName();
        type = directionType;
    }
}
