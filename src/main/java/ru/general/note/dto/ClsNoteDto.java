package ru.general.note.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.general.note.model.ClsComplexityPurpose;
import ru.general.note.model.ClsNote;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClsNoteDto {
    private Long id;
    private String name;
    private String results;
    private ClsComplexityPurpose complexity;
    private ClsPriorityDto priority;
    private ClsPurposeDto purpose;
    private Timestamp dateStart;
    private Time timeStart;
    private Time deadline;
    private String reasonsForDoing;
    private Boolean isDone;
    private Boolean isDefault;
    private Date wastedTime;
    private Boolean isSuspended;
    private LocalDateTime timeSuspended;
    private Boolean isDeleted;
    private String causeDeleted;

    public ClsNoteDto(@NonNull ClsNote note, ClsTypeDirectionDto typeDirection) {
        id = note.getId();
        name = note.getName();
        results = note.getResults();
        complexity = note.getComplexity();
        priority = new ClsPriorityDto(note.getPriority());
        purpose = new ClsPurposeDto(note.getPurpose(), typeDirection);
        dateStart = note.getDateStart();
        timeStart = note.getTimeStart();
        deadline = note.getDeadline();
        reasonsForDoing = note.getReasonsForDoing();
        isDone = note.getIsDone();
        isDefault = note.getIsDefault();
        wastedTime = note.getWastedTime();
        isSuspended = note.getIsSuspended();
        timeSuspended = note.getTimeSuspended();
        isDeleted = note.getIsDeleted();
        causeDeleted = note.getCauseDeleted();
    }
}
