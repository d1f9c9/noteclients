package ru.general.note.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.general.note.model.ClsTypeDirection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClsTypeDirectionDto {
    private Long id;
    private String name;
    private String code;

    public ClsTypeDirectionDto(ClsTypeDirection direction) {
        if (direction != null) {
            id = direction.getId();
            name = direction.getName();
            code = direction.getCode();
        }
    }
}
