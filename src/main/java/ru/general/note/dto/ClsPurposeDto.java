package ru.general.note.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import ru.general.note.model.ClsComplexityPurpose;
import ru.general.note.model.ClsPurpose;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClsPurposeDto {
    private Long id;
    private String name;
    private String results;
    private ClsComplexityPurpose complexity;
    private ClsPriorityDto priority;
    private ClsDirectionDto direction;
    private String timeStart;
    private String deadline;
    private String reasonsForDoing;
    private Boolean isDone;
    private String wastedTime;
    private Boolean isSuspended;
    private LocalDateTime timeSuspended;
    private Boolean isDeleted;
    private String causeDeleted;

    public ClsPurposeDto(@NonNull ClsPurpose purpose, ClsTypeDirectionDto typeDirection) {
        id = purpose.getId();
        name = purpose.getName();
        results = purpose.getResults();
        complexity = purpose.getComplexity();
        priority = new ClsPriorityDto(purpose.getPriority());
        direction = new ClsDirectionDto(purpose.getDirection(), typeDirection);
        timeStart = purpose.getTimeStart().toString();
        deadline = purpose.getDeadline().toString();
        reasonsForDoing = purpose.getReasonsForDoing();
        isDone = purpose.getIsDone();
        wastedTime = purpose.getWastedTime();
        isSuspended = purpose.getIsSuspended();
        timeSuspended = purpose.getTimeSuspended();
        isDeleted = purpose.getIsDeleted();
        causeDeleted = purpose.getCauseDeleted();
    }
}
