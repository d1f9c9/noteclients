package ru.general.note.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.general.note.model.ClsUser;

@Data
@AllArgsConstructor
public class ClsUserDto {
    private Long id;
    private String name;
    private String login;
    private String password;
    private String mail;

    ClsUserDto(ClsUser user) {
        id = user.getId();
        name = user.getName();
        login = user.getLogin();
        mail = user.getMail();
    }
}
