package ru.general.note.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClsNoteFilterDto {
    private String searchNameResult;
    private Long idPriority;
    private Long idComplexity;
    private Long idPurpose;
    private LocalDateTime dateStart;
    private LocalDateTime dateBeforeStart;
    private LocalDateTime dateAfterStart;
    private Time timeStart;
    private Time timeBeforeStart;
    private Time timeAfterStart;
    private Time deadline;
    private Time beforeDeadline;
    private Time afterDeadline;
    private Boolean isDefault;
    private Boolean isDone;
    private Boolean isSuspended;
    private Boolean isDeleted;
}
