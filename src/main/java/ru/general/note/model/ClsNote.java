package ru.general.note.model;

import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import ru.general.note.dto.ClsNoteDto;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "cls_note", schema = "public")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder(toBuilder = true)
@DynamicInsert
@DynamicUpdate
public class ClsNote {
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "CLS_NOTE_SEQ_GEN", sequenceName = "cls_note_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLS_NOTE_SEQ_GEN")
    private Long id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "results")
    private String results;
    @OneToOne
    @JoinColumn(name = "id_complexity")
    ClsComplexityPurpose complexity;
    @OneToOne
    @JoinColumn(name = "id_priority")
    ClsPriority priority;
    @OneToOne
    @JoinColumn(name = "id_purpose")
    ClsPurpose purpose;
    @Basic
    @Column(name = "date_start")
    private Timestamp dateStart;
    @Basic
    @Column(name = "time_create")
    private Timestamp timeCreate;
    @Basic
    @Column(name = "time_start")
    private Time timeStart;
    @Basic
    @Column(name = "deadline")
    private Time deadline;
    @Basic
    @Column(name = "reasons_for_doing")
    private String reasonsForDoing;
    @Basic
    @Column(name = "is_done")
    private Boolean isDone;
    @Basic
    @Column(name = "is_default")
    private Boolean isDefault;
    @Basic
    @Column(name = "wasted_time")
    private Date wastedTime;
    @Basic
    @Column(name = "is_suspended")
    private Boolean isSuspended;
    @Column(name = "time_suspended")
    private LocalDateTime timeSuspended;
    @Basic
    @Column(name = "is_deleted")
    private Boolean isDeleted;
    @Basic
    @Column(name = "cause_deleted")
    private String causeDeleted;

    public ClsNote(ClsNoteDto note) {
        if(note != null) {
            id = note.getId();
            name = note.getName();
            results = note.getResults();
            complexity = note.getComplexity();
            priority = new ClsPriority(note.getPriority());
            purpose = new ClsPurpose(note.getPurpose());
            dateStart = note.getDateStart();
            timeStart = note.getTimeStart();
            deadline = note.getDeadline();
            reasonsForDoing = note.getReasonsForDoing();
            isDone = note.getIsDone();
            isDefault = note.getIsDefault();
            wastedTime = note.getWastedTime();
            isSuspended = note.getIsSuspended();
            timeSuspended = note.getTimeSuspended();
            isDeleted = note.getIsDeleted();
            causeDeleted = note.getCauseDeleted();
        }
    }

    @Override
    public boolean equals(Object object) {
        boolean result = this == object;

        if (!result && object != null && getClass() == object.getClass()) {
            ClsNote that = (ClsNote) object;

            result = Objects.equals(id, that.id)
                    && Objects.equals(name, that.name)
                    && Objects.equals(results, that.results)
                    && Objects.equals(complexity, that.complexity)
                    && Objects.equals(priority, that.priority)
                    && Objects.equals(purpose, that.purpose)
                    && Objects.equals(dateStart, that.dateStart)
                    && Objects.equals(timeCreate, that.timeCreate)
                    && Objects.equals(timeStart, that.timeStart)
                    && Objects.equals(deadline, that.deadline)
                    && Objects.equals(isDefault, that.isDefault)
                    && Objects.equals(reasonsForDoing, that.reasonsForDoing)
                    && Objects.equals(isDone, that.isDone)
                    && Objects.equals(wastedTime, that.wastedTime)
                    && Objects.equals(isSuspended, that.isSuspended)
                    && Objects.equals(timeSuspended, that.timeSuspended)
                    && Objects.equals(isDeleted, that.isDeleted)
                    && Objects.equals(causeDeleted, that.causeDeleted);
        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, results, complexity, priority, purpose, dateStart, timeCreate, timeStart, deadline, isDefault, reasonsForDoing, isDone, wastedTime, isSuspended, timeSuspended, isDeleted, causeDeleted);
    }

}
