package ru.general.note.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.general.note.dto.ClsTypeDirectionDto;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cls_type_direction", schema = "public")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ClsTypeDirection {
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "CLS_TYPE_DIRECTION_SEQ_GEN", sequenceName = "cls_type_direction_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLS_TYPE_DIRECTION_SEQ_GEN")
    private Long id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "code")
    private String code;

    ClsTypeDirection(ClsTypeDirectionDto typeDirection) {
        if(typeDirection != null) {
            id = typeDirection.getId();
            name = typeDirection.getName();
            code = typeDirection.getCode();
        }
    }

    @Override
    public boolean equals(Object object) {
        boolean result = this == object;

        if (!result && object != null && getClass() == object.getClass()) {
            ClsTypeDirection that = (ClsTypeDirection) object;

            result = Objects.equals(id, that.id)
                    && Objects.equals(name, that.name)
                    && Objects.equals(code, that.code);
        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, code);
    }
}
