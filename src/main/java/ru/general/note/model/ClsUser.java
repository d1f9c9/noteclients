package ru.general.note.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.general.note.dto.ClsTypeDirectionDto;
import ru.general.note.dto.ClsUserDto;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cls_user", schema = "public")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder(toBuilder = true)
public class ClsUser {
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "CLS_USER_SEQ_GEN", sequenceName = "cls_user_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLS_USER_SEQ_GEN")
    private Long id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "login")
    private String login;
    @Basic
    @Column(name = "password")
    private String password;

    @Basic
    @Column(name = "mail")
    private String mail;

//
//    ClsUser(ClsUserDto user) {
//        if(typeDirection != null) {
//            id = typeDirection.getId();
//            name = typeDirection.getName();
//            code = typeDirection.getCode();
//        }
//    }

    @Override
    public boolean equals(Object object) {
        boolean result = this == object;

        if (!result && object != null && getClass() == object.getClass()) {
            ClsUser that = (ClsUser) object;

            result = Objects.equals(id, that.id)
                    && Objects.equals(name, that.name)
                    && Objects.equals(login, that.login)
                    && Objects.equals(password, that.password);

        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, login, password);
    }
}
