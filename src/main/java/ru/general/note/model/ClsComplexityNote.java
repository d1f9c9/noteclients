package ru.general.note.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "cls_complexity_note", schema = "public")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ClsComplexityNote implements Serializable {
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "CLS_COMPLEXITY_NOTE_SEQ_GEN", sequenceName = "cls_complexity_note_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLS_COMPLEXITY_NOTE_SEQ_GEN")
    private Long id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "note")
    private String note;
    @Basic
    @Column(name = "code")
    private String code;

    @Override
    public boolean equals(Object object) {
        boolean result = this == object;

        if(!result && object != null && getClass() == object.getClass()) {
            ClsComplexityNote that = (ClsComplexityNote) object;

            result = Objects.equals(id, that.id)
                    && Objects.equals(name, that.name)
                    && Objects.equals(note, that.note)
                    && Objects.equals(code, that.code);
        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, note, code);
    }

}
