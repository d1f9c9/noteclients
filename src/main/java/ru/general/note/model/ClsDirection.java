package ru.general.note.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.general.note.dto.ClsDirectionDto;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "cls_direction", schema = "public")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ClsDirection {
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "CLS_DIRECTION_SEQ_GEN", sequenceName = "cls_direction_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLS_DIRECTION_SEQ_GEN")
    private Long id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "time_create")
    private LocalDateTime timeCreate;
    @OneToOne
    @JoinColumn(name = "id_type_direction")
    private ClsTypeDirection typeDirection;

    public ClsDirection(ClsDirectionDto direction) {
        if (direction != null) {
            id = direction.getId();
            name = direction.getName();
            typeDirection = new ClsTypeDirection(direction.getType());
        }
    }

    @Override
    public boolean equals(Object object) {
        boolean result = this == object;

        if (!result && object != null && getClass() == object.getClass()) {
            ClsDirection that = (ClsDirection) object;

            result = Objects.equals(id, that.id)
                    && Objects.equals(name, that.name)
                    && Objects.equals(timeCreate, that.timeCreate)
                    && Objects.equals(typeDirection, that.typeDirection);
        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, timeCreate, typeDirection);
    }
}
