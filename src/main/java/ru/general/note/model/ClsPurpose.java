package ru.general.note.model;

import lombok.*;
import ru.general.note.dto.ClsPurposeDto;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "cls_purpose", schema = "public")
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Getter
@Setter
public class ClsPurpose {
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "CLS_PURPOSE_SEQ_GEN", sequenceName = "cls_purpose_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLS_PURPOSE_SEQ_GEN")
    private Long id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "results")
    private String results;
    @OneToOne
    @JoinColumn(name = "id_complexity")
    ClsComplexityPurpose complexity;
    @OneToOne
    @JoinColumn(name = "id_priority")
    ClsPriority priority;
    @OneToOne
    @JoinColumn(name = "id_direction")
    ClsDirection direction;
    @Basic
    @Column(name = "time_create")
    private Timestamp timeCreate;
    @Basic
    @Column(name = "time_start")
    private Date timeStart;
    @Basic
    @Column(name = "deadline")
    private Date deadline;
    @Basic
    @Column(name = "reasons_for_doing")
    private String reasonsForDoing;
    @Basic
    @Column(name = "is_done")
    private Boolean isDone;
    @Basic
    @Column(name = "wasted_time")
    private String wastedTime;
    @Basic
    @Column(name = "is_suspended")
    private Boolean isSuspended;
    @Column(name = "time_suspended")
    private LocalDateTime timeSuspended;
    @Basic
    @Column(name = "is_deleted")
    private Boolean isDeleted;
    @Basic
    @Column(name = "cause_deleted")
    private String causeDeleted;

    public ClsPurpose(ClsPurposeDto purpose) {
        if (purpose != null) {
            id = purpose.getId();
            name = purpose.getName();
            results = purpose.getResults();
            complexity = purpose.getComplexity();
            priority = new ClsPriority(purpose.getPriority());
            direction = new ClsDirection(purpose.getDirection());
            timeStart = Date.valueOf(purpose.getTimeStart());
            deadline = Date.valueOf(purpose.getDeadline());
            reasonsForDoing = purpose.getReasonsForDoing();
            isDone = purpose.getIsDone();
            wastedTime = purpose.getWastedTime();
            isSuspended = purpose.getIsSuspended();
            timeSuspended = purpose.getTimeSuspended();
            isDeleted = purpose.getIsDeleted();
            causeDeleted = purpose.getCauseDeleted();
        }
    }

    @Override
    public boolean equals(Object object) {
        boolean result = this == object;

        if (!result && object != null && getClass() == object.getClass()) {
            ClsPurpose that = (ClsPurpose) object;

            result = Objects.equals(id, that.id)
                    && Objects.equals(name, that.name)
                    && Objects.equals(results, that.results)
                    && Objects.equals(complexity, that.complexity)
                    && Objects.equals(priority, that.priority)
                    && Objects.equals(direction, that.direction)
                    && Objects.equals(timeCreate, that.timeCreate)
                    && Objects.equals(deadline, that.deadline)
                    && Objects.equals(reasonsForDoing, that.reasonsForDoing)
                    && Objects.equals(isDone, that.isDone)
                    && Objects.equals(wastedTime, that.wastedTime)
                    && Objects.equals(isSuspended, that.isSuspended)
                    && Objects.equals(timeSuspended, that.timeSuspended)
                    && Objects.equals(isDeleted, that.isDeleted)
                    && Objects.equals(causeDeleted, that.causeDeleted);
        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, results, complexity, priority, direction, timeCreate, deadline, reasonsForDoing, isDone, wastedTime, isSuspended, timeSuspended, isDeleted, causeDeleted);
    }

}
