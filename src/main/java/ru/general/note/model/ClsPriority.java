package ru.general.note.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.general.note.dto.ClsPriorityDto;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cls_priority", schema = "public")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ClsPriority {
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "CLS_PRIORITY_SEQ_GEN", sequenceName = "cls_priority_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLS_PRIORITY_SEQ_GEN")
    private Long id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "code")
    private String code;

    public ClsPriority(ClsPriorityDto priority) {
        if (priority != null) {
            id = priority.getId();
            name = priority.getName();
            code = priority.getCode();
        }
    }

    @Override
    public boolean equals(Object object) {
        boolean result = this == object;

        if (!result && object != null && getClass() == object.getClass()) {
            ClsPriority that = (ClsPriority) object;

            result = Objects.equals(id, that.id)
                    && Objects.equals(name, that.name)
                    && Objects.equals(code, that.code);
        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, code);
    }
}
