package ru.general.note.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.general.note.dto.ClsTypeDirectionDto;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "reg_user_role", schema = "public")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class RegUserRole {
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "REG_USER_ROEL_SEQ_GEN", sequenceName = "reg_user_roel_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REG_USER_ROLE_SEQ_GEN")
    private Long id;

    @OneToOne
    @JoinColumn(name = "id_user")
    private ClsUser user;

    @OneToOne
    @JoinColumn(name = "id_role")
    private ClsRole role;

    @Override
    public boolean equals(Object object) {
        boolean result = this == object;

        if (!result && object != null && getClass() == object.getClass()) {
            RegUserRole that = (RegUserRole) object;

            result = Objects.equals(id, that.id)
                    && Objects.equals(user, that.user)
                    && Objects.equals(role, that.role);

        }

        return result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, role);
    }
}
